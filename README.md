presentation link: https://www.youtube.com/watch?v=-AW5BKYZwx4

Build an Express application that:
a) Handle the path / hello by returning "hello world"
b) Handle the path / form by returning an HTML form with 3 fields, the form should send data under / formdata
c) Will handle the path / formdata accepting the form data and display them in a civilized way using the selected engine templates
d) Handle the path / jsondata by accepting data in json format (use e.g. curl to test) and display it in a civilized way using the selected engine templates.
The data structure depends on you, but json should have at least 3 fields