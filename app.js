const express = require("express");
const bodyParser = require("body-parser");

const app = express();
app.set("view engine", "pug");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true })); //? + &

var d = null;

var title = "Hello World...";
var imie;
var nazwisko;
var wiek;

var product;
var quantity;
var prize;

app.get("/form", function(request, response) {
  let bod = request.body;
  console.log(bod.product);

  return response.render("form-with-get");
});

app.get("/form-post", function(request, response) {
  return response.render("form-with-post");
});

app.get("/submit-form-with-get", function(request, response) {
  d = request.query;
  product = d.product;
  quantity = d.quantity;
  prize = d.prize;

  return response.send("INFO: DATA SENT");
});

app.get("/formdata", function(request, response) {
  return response.render("data", {
    product: `${product}`,
    quantity: `${quantity}`,
    prize: `${prize}`
  });
});

app.post("/json", function(request, response) {
  let bod = request.body;
  imie = bod.imie;
  nazwisko = bod.nazwisko;
  wiek = bod.wiek;

  response.send(request.body);
});

app.get("/json", function(request, response) {
  return response.render("json", {
    imie: `${imie}`,
    nazwisko: `${nazwisko}`,
    wiek: `${wiek}`
  });
});

app.get("/hello", function(req, resp) {
  return resp.render("hello", { title });
});

app.listen(3000, function() {
  console.log("server started on port 3000");
});
